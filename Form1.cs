﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WorkingGraph
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            if(Properties.Settings.Default.conf_b == false)
            {
                PreConfig p = new PreConfig();
                p.ShowDialog();
                Properties.Settings.Default.conf_b = true;
            }

            DateTime sourse = Properties.Settings.Default.sourse;
            int index = Properties.Settings.Default.index;
            calculate(sourse, index);
            
        }

        void calculate (DateTime sourse, int index)
        {
            DateTime now = DateTime.Now.Date;
            int dif = (now - sourse).Days;
            int k = dif % 4;
            k += index;
            if (k > 4)
                k -= 4;
            show_message(k);
            Properties.Settings.Default.index = k;
            Properties.Settings.Default.sourse = now;
            Properties.Settings.Default.Save();
        }

        void show_message(int k)
        {
            switch (k)
            {
                case 1:
                    {
                        label1.Text = "В День";
                        label1.ForeColor = Color.Black;
                        break;
                    }
                case 2:
                    {
                        label1.Text = "В ночь";
                        label1.ForeColor = Color.Black;
                        break;
                    }
                case 3:
                    {
                        label1.Text = "С ночи";
                        label1.ForeColor = Color.Red;
                        break;
                    }
                case 4:
                    {
                        label1.Text = "Дома";
                        label1.ForeColor = Color.Red;
                        break;
                    }
                default:
                    {
                        label1.Text = "ERROR";
                        break;
                    }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PreConfig p = new PreConfig();
            p.ShowDialog();
            calculate(Properties.Settings.Default.sourse, Properties.Settings.Default.index);

        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.Visible = false;
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if((e.X > 235 && e.X <272) && (e.Y>7 && e.Y <38))
                button1.Visible = true;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Calendar c = new Calendar();
            c.ShowDialog();
        }
    }
}
