﻿namespace WorkingGraph
{
    partial class Calendar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DaysGrid = new System.Windows.Forms.DataGridView();
            this.Monday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tuesday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Wednesday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Thursday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Friday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Saturday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sunday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.curr_m = new System.Windows.Forms.Label();
            this.next_m = new System.Windows.Forms.Button();
            this.prev_m = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DaysGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // DaysGrid
            // 
            this.DaysGrid.AllowUserToAddRows = false;
            this.DaysGrid.AllowUserToDeleteRows = false;
            this.DaysGrid.AllowUserToResizeColumns = false;
            this.DaysGrid.AllowUserToResizeRows = false;
            this.DaysGrid.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DaysGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DaysGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.DaysGrid.ColumnHeadersHeight = 20;
            this.DaysGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.DaysGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Monday,
            this.Tuesday,
            this.Wednesday,
            this.Thursday,
            this.Friday,
            this.Saturday,
            this.Sunday});
            this.DaysGrid.Location = new System.Drawing.Point(0, 30);
            this.DaysGrid.MultiSelect = false;
            this.DaysGrid.Name = "DaysGrid";
            this.DaysGrid.ReadOnly = true;
            this.DaysGrid.RowHeadersVisible = false;
            this.DaysGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DaysGrid.RowTemplate.Height = 20;
            this.DaysGrid.RowTemplate.ReadOnly = true;
            this.DaysGrid.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DaysGrid.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.DaysGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.ColumnHeaderSelect;
            this.DaysGrid.Size = new System.Drawing.Size(175, 120);
            this.DaysGrid.TabIndex = 0;
            // 
            // Monday
            // 
            this.Monday.HeaderText = "Пн";
            this.Monday.Name = "Monday";
            this.Monday.ReadOnly = true;
            this.Monday.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Monday.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Monday.ToolTipText = "Понелельник";
            this.Monday.Width = 25;
            // 
            // Tuesday
            // 
            this.Tuesday.HeaderText = "Вт";
            this.Tuesday.Name = "Tuesday";
            this.Tuesday.ReadOnly = true;
            this.Tuesday.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Tuesday.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Tuesday.ToolTipText = "Вторник";
            this.Tuesday.Width = 25;
            // 
            // Wednesday
            // 
            this.Wednesday.HeaderText = "Ср";
            this.Wednesday.Name = "Wednesday";
            this.Wednesday.ReadOnly = true;
            this.Wednesday.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Wednesday.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Wednesday.ToolTipText = "Среда";
            this.Wednesday.Width = 25;
            // 
            // Thursday
            // 
            this.Thursday.HeaderText = "Чт";
            this.Thursday.Name = "Thursday";
            this.Thursday.ReadOnly = true;
            this.Thursday.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Thursday.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Thursday.ToolTipText = "Четверг";
            this.Thursday.Width = 25;
            // 
            // Friday
            // 
            this.Friday.HeaderText = "Пт";
            this.Friday.Name = "Friday";
            this.Friday.ReadOnly = true;
            this.Friday.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Friday.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Friday.ToolTipText = "Пятница";
            this.Friday.Width = 25;
            // 
            // Saturday
            // 
            this.Saturday.HeaderText = "Сб";
            this.Saturday.Name = "Saturday";
            this.Saturday.ReadOnly = true;
            this.Saturday.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Saturday.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Saturday.ToolTipText = "Суббота";
            this.Saturday.Width = 25;
            // 
            // Sunday
            // 
            this.Sunday.HeaderText = "Вс";
            this.Sunday.Name = "Sunday";
            this.Sunday.ReadOnly = true;
            this.Sunday.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Sunday.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Sunday.ToolTipText = "Воскресенье";
            this.Sunday.Width = 25;
            // 
            // curr_m
            // 
            this.curr_m.AutoSize = true;
            this.curr_m.Location = new System.Drawing.Point(42, 8);
            this.curr_m.Name = "curr_m";
            this.curr_m.Size = new System.Drawing.Size(35, 13);
            this.curr_m.TabIndex = 3;
            this.curr_m.Text = "label1";
            // 
            // next_m
            // 
            this.next_m.BackgroundImage = global::WorkingGraph.Properties.Resources.right;
            this.next_m.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.next_m.Location = new System.Drawing.Point(136, 3);
            this.next_m.Name = "next_m";
            this.next_m.Size = new System.Drawing.Size(24, 24);
            this.next_m.TabIndex = 2;
            this.next_m.UseVisualStyleBackColor = true;
            this.next_m.Click += new System.EventHandler(this.next_m_Click);
            // 
            // prev_m
            // 
            this.prev_m.BackColor = System.Drawing.Color.Transparent;
            this.prev_m.BackgroundImage = global::WorkingGraph.Properties.Resources.left;
            this.prev_m.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.prev_m.Location = new System.Drawing.Point(12, 3);
            this.prev_m.Name = "prev_m";
            this.prev_m.Size = new System.Drawing.Size(24, 24);
            this.prev_m.TabIndex = 1;
            this.prev_m.UseVisualStyleBackColor = false;
            this.prev_m.Click += new System.EventHandler(this.prev_m_Click);
            // 
            // Calendar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(175, 149);
            this.Controls.Add(this.curr_m);
            this.Controls.Add(this.next_m);
            this.Controls.Add(this.prev_m);
            this.Controls.Add(this.DaysGrid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Calendar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Календарь";
            ((System.ComponentModel.ISupportInitialize)(this.DaysGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DaysGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Monday;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tuesday;
        private System.Windows.Forms.DataGridViewTextBoxColumn Wednesday;
        private System.Windows.Forms.DataGridViewTextBoxColumn Thursday;
        private System.Windows.Forms.DataGridViewTextBoxColumn Friday;
        private System.Windows.Forms.DataGridViewTextBoxColumn Saturday;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sunday;
        private System.Windows.Forms.Button prev_m;
        private System.Windows.Forms.Button next_m;
        private System.Windows.Forms.Label curr_m;
    }
}