﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WorkingGraph
{
    public partial class Calendar : Form
    {
        DateTime date;
        string[] Month = { "Error","Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" };
        enum fs { Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday };
        
        public Calendar()
        {
            InitializeComponent();

            date = DateTime.Now.Date;
            curr_m.Text = Month[date.Month] + " " + date.Year.ToString()+" г.";
            calculate_cal();
        }

        private void next_m_Click(object sender, EventArgs e)
        {
            date = date.AddMonths(1);
            curr_m.Text = Month[date.Month] + " " + date.Year.ToString()+" г.";
            calculate_cal();
        }

        void calculate_cal()
        {
            int index = Properties.Settings.Default.index;

            DaysGrid.Rows.Clear();

            DateTime tmp = new DateTime(date.Year, date.Month, 1);
            int days = DateTime.DaysInMonth(date.Year, date.Month);
            int d = 1;
            
            DayOfWeek v = tmp.DayOfWeek;
            int dow = get_day_of_week(v) - 1;
            int row = 0;
            DaysGrid.Rows.Add();

            for (d=1; d<=days; d++)
            {
                if(dow>6)
                {
                    DaysGrid.Rows.Add();
                    dow = 0;
                    row++;
                }
                int coef = calculate(tmp, index);
                if (coef == 3 || coef == 4)
                    DaysGrid.Rows[row].Cells[dow].Style.BackColor = Color.Red; ;

                DaysGrid.Rows[row].Cells[dow].Value = d;
                tmp = tmp.AddDays(1);
                dow++;
            }

            if (row==5 && this.Height!=208)
            {
                this.Height += 20;
                DaysGrid.Height += 20;
            }
            else if(row<5 && this.Height==208)
            {
                this.Height -= 20;
                DaysGrid.Height -= 20;
            }

            DaysGrid.ClearSelection();
        }

        public int calculate(DateTime dst, int index)
        {
            DateTime now = DateTime.Now.Date;
            int dif = (dst-now).Days;
            if(dif<0)
            {
                dif = Math.Abs(dif) + 1;
            }
            int k = dif % 4;
            k += index;
            if (k > 4)
                k -= 4;
            return k;
        }

        private void prev_m_Click(object sender, EventArgs e)
        {
            date = date.AddMonths(-1);
            curr_m.Text = Month[date.Month] + " " + date.Year.ToString() + " г.";
            calculate_cal();
        }

        private int get_day_of_week(DayOfWeek dow)
        {
            switch (dow)
            {
                case DayOfWeek.Monday:
                    return 1;
                case DayOfWeek.Tuesday:
                    return 2;
                case DayOfWeek.Wednesday:
                    return 3;
                case DayOfWeek.Thursday:
                    return 4;
                case DayOfWeek.Friday:
                    return 5;
                case DayOfWeek.Saturday:
                    return 6;
                case DayOfWeek.Sunday:
                    return 7;
                default:
                    return 0;
            }
        }
    }
}
