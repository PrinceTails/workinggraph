﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WorkingGraph
{
    public partial class PreConfig : Form
    {
        public PreConfig()
        {
            InitializeComponent();
            comboBox1.Items.Add("В день");
            comboBox1.Items.Add("В ночь");
            comboBox1.Items.Add("С ночи");
            comboBox1.Items.Add("Дома");
            Properties.Settings.Default.sourse = DateTime.Now.Date;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.index = comboBox1.SelectedIndex + 1;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.sourse = dateTimePicker1.Value.Date;
        }
    }
}
